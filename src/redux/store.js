import {combineReducers, createStore} from "redux";
import toDoReducer from "./ToDoReducer";
import {reducer as formReducer} from "redux-form"

let rootReducer = combineReducers({
    tasksState: toDoReducer,
    form: formReducer
})
let store = createStore(rootReducer)

export default store