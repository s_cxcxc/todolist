const ADD_TASK = "ADD_TASK";
const CHANGE_TASK_START = "CHANGE_TASK";
const DELETE_TASK = "DELETE_TASK";
const CHANGE_DONE_STATUS = "CHANGE_DONE_STATUS";
const CHANGE_TASK_END = "CHANGE_TASK_END";
const ALL = "ALL";
const DONE = "DONE";
const UN_DONE = "UN_DONE";
const CHANGE_FILTER = "CHANGE_FILTER";

const initialState = {
    tasks: [],
    idCount: 0,
    filter: ALL
}

const mapAndChange = (state, action) =>( mapField, changeField ) => {
    let startArray = [...state[mapField]];
    let newArray = startArray.map(el => {
        if(el.id === action.id){
            return {...el, [changeField]:!el[changeField]}
        }
        else return el
    });
    return newArray
}

const toDoReducer = (state = initialState, action) => {

    let tasks = [];
    let updatedTasks = [];
    switch (action.type) {
        case ADD_TASK:
            let task = {...action.task, id: state.idCount, isDone: false, isEdited: false};
            return {
                ...state, tasks: [...state.tasks, task], idCount: state.idCount + 1
            };
        case DELETE_TASK:
            debugger;
            tasks = [...state.tasks];
            for (let i = 0; i < tasks.length; i++) {
                if (tasks[i].id != action.id) {
                    updatedTasks.push(tasks[i])
                }
            }
            return {...state, tasks: [...updatedTasks]};
        case CHANGE_DONE_STATUS:
            return {...state, tasks:mapAndChange(state,action)("tasks","isDone")};
        case CHANGE_TASK_START:
            return {...state, tasks:mapAndChange(state,action)("tasks","isEdited")};
        case CHANGE_TASK_END:
            tasks = [...state.tasks];
            updatedTasks = tasks.map(task => task.id === action.id ? {...action.task, isEdited: false} : task)
            return {...state, tasks:updatedTasks};
        case CHANGE_FILTER:
            return {...state, filter: action.filter};
        default:
            return state
    }
}

export const changeDoneStatus = id => ({type: CHANGE_DONE_STATUS, id});
export const addTask = task => ({type: ADD_TASK, task});
export const deleteTask = id => ({type: DELETE_TASK, id});
export const changeTaskStart = id => ({type: CHANGE_TASK_START,id});
export const changeTaskEnd = (id, task) => ({type: CHANGE_TASK_END, id, task});
export const changeFilter = (filter) => ({type: CHANGE_FILTER, filter});

export const getTasks = (tasks, filter) => {
    switch (filter) {
        case DONE:
            return tasks.filter(task => task.isDone);
        case UN_DONE:
            return tasks.filter(task => !task.isDone);
        case ALL:
            return tasks

    }
}

export default toDoReducer