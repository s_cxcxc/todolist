import React from "react"
import ToDoForm from "./ToDoForm";
import {connect} from "react-redux";
import {addTask, changeFilter} from "../../redux/ToDoReducer";
import {reset} from "redux-form";

const ToDoFormContainer = ({addTask,reset,...props}) => {
    const onSubmit = values => {
        debugger
        addTask(values)
        reset("toDoForm")
    }
    return (
        <ToDoForm onSubmit={onSubmit}{...props}/>
    )
}
const mapStateToProps = state => ({})
export default connect(mapStateToProps,{addTask,reset, changeFilter})(ToDoFormContainer)