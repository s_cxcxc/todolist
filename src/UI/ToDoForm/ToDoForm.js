import React from "react"
import {Field, reduxForm} from "redux-form";
import styled from "styled-components"
const ALL = "ALL";
const DONE = "DONE";
const UN_DONE = "UN_DONE";


const FormContainer = styled.div`
display: grid
grid-template-rows: 1.5em 4em 1.5em 2em
gap: 2em
margin: 1em 15%
`
const ButtonContainer = styled.div`
display: grid
grid-template-columns: 3fr 4fr 2fr 
`
const AddButton = styled.button`
grid-column-start:3
`
const RadioButtonContainer = styled.div`
display: grid
grid-template-columns:1fr 1fr 1fr
`
const RadioButton = styled.input`

`
const TitleInput = styled.input`
border: 1px solid white
border-radius: 2px
background:rgba(0,0,0,0.3);
color: white
& :hover,:active,:focus {
outline: none
}
`
const DescriptionTextArea = styled.textarea`
resize: none
border: 1px solid white
border-radius: 2px
background:rgba(81, 0, 91,0.5);
color: white
& :hover,:active,:focus {
outline: none
}
`

const Input = ({input, meta, ...props}) => {
    return (
        <TitleInput{...props}{...input}/>
    )
}
const Radio = ({input, meta, ...props}) => {
    return (
        <input type="radio"{...props}{...input}/>
    )
}
const TextArea = ({input, meta, ...props}) => {
    return (
        <DescriptionTextArea{...props}{...input}/>
    )
}

let ToDoForm = ({handleSubmit, changeFilter}) => {
    return (
        <form onSubmit={handleSubmit}>
            <FormContainer>
                <Field component={Input} name="title" type="text" placeholder="title"/>
                <Field component={TextArea} name="description" type="text" placeholder="description"/>
                <Field component={Input} name="deadline" type="date"/>
                <ButtonContainer>
                    <RadioButtonContainer>
                        <label>all</label>
                        <input type="radio" name="filter" onClick={()=>changeFilter(ALL)}/>
                        <label>done</label>
                        <input type="radio" name="filter" onClick={()=>changeFilter(DONE)}/>

                        <input type="radio" name="filter" onClick={()=>changeFilter(UN_DONE)}/>
                        <label>not done</label>
                    </RadioButtonContainer>
                    <AddButton type="submit">Add task</AddButton>
                </ButtonContainer>
            </FormContainer>
        </form>

    )
}


export default reduxForm({form: "toDoForm"})(ToDoForm)