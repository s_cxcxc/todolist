import React from "react"
import Tasks from "./Tasks/Tasks";
import styled from "styled-components"


const Table = styled.div`
display: grid
grid-auto-rows: 2em
`

const ToDoList = (props) => {
    return (
        <Table>
            <Tasks {...props}/>
        </Table>

    )
}
export default ToDoList