import React from 'react'
import {Field, reduxForm} from "redux-form";
import styled from "styled-components";

const TaskContainer = styled.div`
display: grid
grid-template-columns: 1em 1fr 1fr 1fr 2em 2em
`
const EditableTask = ({initialValues, handleSubmit, deleteTask, changeDoneStatus, changeTaskStart}) => {
    debugger
    return (
        <>
            <form onSubmit={handleSubmit}>
                <TaskContainer>
                    <Field component="input" type="checkbox" name="isDone">
                    </Field>
                    <Field component="input" type="text" name="title">
                    </Field>
                    <Field component="input" type="text" name="description">
                    </Field>
                    <Field component="input" type="date" name="deadline">
                    </Field>
                    <button type="button" onClick={() => deleteTask(initialValues.id)}>delete</button>
                    <button type="submit">complete</button>
                </TaskContainer>
            </form>
        </>
    )
}
export default reduxForm({form: "editedTask"})(EditableTask)