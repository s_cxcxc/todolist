import React from "react"
import Task from "./Task/task";
import EditableTask from "./EditableTask/EditableTask";


const Tasks = ({tasks, deleteTask, changeDoneStatus, changeTaskStart, changeTaskEnd}) => {
    const onSubmit = values => {
        changeTaskEnd(values.id, values)
    }
    debugger
    let tasksList = tasks.map(task => {
        if(!task.isEdited)
            return <Task task={task} deleteTask={deleteTask} changeDoneStatus={changeDoneStatus} changeTaskStart={changeTaskStart}
                  key={task.id}/>
        else return <EditableTask initialValues={task} onSubmit={onSubmit} deleteTask={deleteTask} changeDoneStatus={changeDoneStatus} changeTaskStart={changeTaskStart} key={task.id} />
        }
    )
    return (

        <>
            {tasksList}
        </>

    )
}
export default Tasks