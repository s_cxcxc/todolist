import React from "react"
import styled from "styled-components"

const FromField = styled.div`
text-align: center
`
const TaskContainer = styled.div`
display: grid
grid-template-columns: 1em 1fr 1fr 1fr 2em 2em
`

const Task = ({task, deleteTask, changeDoneStatus, changeTaskStart}) => {
    let {title, description, deadline, id, isDone} = task;
    return (
        <TaskContainer>
            <FromField>
                <input type="checkbox" checked={isDone} onClick={() => changeDoneStatus(id)}/>
            </FromField>
            <FromField>
                {title}
            </FromField>
            <FromField>
                {description}
            </FromField>
            <FromField>
                {deadline}
            </FromField>
            <FromField>
                <button onClick={() => {
                    deleteTask(id)
                }}> delete
                </button>
            </FromField>
            <FromField>
                <button onClick={() => {
                    changeTaskStart(id)
                }}>
                    edit
                </button>
            </FromField>
            </TaskContainer>
            )
            }
            export default Task