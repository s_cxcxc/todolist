import React from "react"
import {connect} from "react-redux";
import ToDoList from "./ToDoList";
import {changeDoneStatus, changeTaskEnd, changeTaskStart, deleteTask, getTasks} from "../../redux/ToDoReducer";


const mapStateToProps = state => ({tasks: getTasks(state.tasksState.tasks, state.tasksState.filter)})

export default connect(mapStateToProps,{deleteTask, changeDoneStatus, changeTaskStart, changeTaskEnd})(ToDoList)