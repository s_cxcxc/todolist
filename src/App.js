import React from "react"
import ToDoFormContainer from "./UI/ToDoForm/ToDoFormContainer";
import ToDoListContainer from "./UI/ToDoList/ToDoListContainer";
import style from "./bodyStyle.css"
import styled from "styled-components"

const ContentContainer = styled.div`
margin-top:3.5em
display: grid
width: 100%
height: 20em
border-radius: 5px
background: rgba(0,0,0,0.1); 
grid-column-start: 2
grid-column-end: 3
`
const Wrapper = styled.div`
display: grid
width: 100%
height: 100vh
grid-template-columns: 1fr 50em 1fr

`
const App = (props) => {
    return (
        <Wrapper>
            <ContentContainer>
                <ToDoFormContainer/>
            </ContentContainer>
            <ContentContainer>
                <ToDoListContainer/>
            </ContentContainer>
        </Wrapper>
    )
}
export default App